<?php

namespace App\Repository;

use App\Classe\Search;
use App\Entity\Product;
use App\Entity\SearchProduct;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Product|null find($id, $lockMode = null, $lockVersion = null)
 * @method Product|null findOneBy(array $criteria, array $orderBy = null)
 * @method Product[]    //findAll()
 * @method Product[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ProductRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Product::class);
    }
    /**
    * @return Product[] Returns an array of Product objects
    */
    public function findWithSearch($search)
    {
        $query = $this->createQueryBuilder('p');
        if ($search->getMinPrice()) {
            $query = $query->andWhere('p.price > ' . $search->getMinPrice() * 100)
                           ->orderBy('p.name', 'ASC');
        }
        if ($search->getMaxPrice()) {
            $query = $query->andWhere('p.price < ' . $search->getMaxPrice() * 100)
                           ->orderBy('p.name', 'ASC');
        }
        if ($search->getTags()) {
            $query = $query->andWhere('p.tags like :val')
                           ->setParameter('val', "%{$search->getTags()}%")
                           ->orderBy('p.name', 'ASC');
        }
        if ($search->getCategories()) {
            $query = $query->select('c', 'p')
                           ->join('p.category', 'c')
                           ->andWhere('c.id IN (:categories)')
                           ->setParameter('categories', $search->getCategories());
        }
            return $query->getQuery()->getResult();
            //dd($query->getQuery()->getResult());
    }
    /*public function findWithSearch(Search $search)
    {
        $query = $this
                ->createQueryBuilder('p')
                ->select('c', 'p')
                ->join('p.category', 'c');
                //->orderBy('c.name', 'ASC')
                //->orderBy('p.name', 'ASC');
        if (!empty($search->categories)) {
            $query = $query
                    ->andWhere('c.id IN (:categories)')
                    ->setParameter('categories', $search->categories);
        }
        return $query->getQuery()->getResult();
    }*/
    public function findAll()
    {
        return $this->findBy([], ['name' => 'ASC']);
    }
    /*public function findBest()
    {
        return $this->findBy([], ['name' => 'ASC']);
    }*/
    // /**
    //  * @return Product[] Returns an array of Product objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('p.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Product
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
